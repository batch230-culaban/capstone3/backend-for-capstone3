const auth = require("../auth.js");
const mongoose = require("mongoose");
const Products = require("../models/products");
const Users = require("../models/users.js");

// add to cart
module.exports.addOrderAndUpdateProduct = async (req, res) => {
  try {
    const usersData = auth.decode(req.headers.authorization);
    if(usersData.isAdmin === true){
      return false
    }
    
    const user = await Users.findById(usersData.id);
    const product = await Products.findById(req.body.productId);

    if (!user || !product) {
      return res.send(false);
    }

    const quantity = parseInt(req.body.quantity);
    const subTotal = product.price * req.body.quantity;

    if (product.stocks < quantity) {
      return res.send(false);
    }

    const order = {
      productId: product._id,
      productName: product.name,
      quantity: req.body.quantity,
      subTotal: subTotal
    };

    // Add the order to the user's myOrders array and update the product order details
    user.myOrders.push({ products: [order], subTotal: subTotal });
    product.orderDetails.push({ userEmail: user.email, quantity: req.body.quantity, subTotal: subTotal });

    user.totalAmount += subTotal;
    product.stocks -= quantity;

    await Promise.all([user.save(), product.save()]);
    console.log(user.totalAmount);
    console.log(subTotal);
    return res.send({ success: true, updatedQuantity: quantity, updatedStock: product.stocks , totalAmount: user.totalAmount, subTotal: subTotal});
  } catch (error) {
    console.error(error);
    return res.send({ success: false, updatedQuantity: null, updatedStock: null });
  }
};

// deleting an Order
module.exports.deleteOrder = async (req, res) => {
  try {
    const { userId, orderId } = req.params;

    // Find the user by user id
    const user = await Users.findById(userId);

    if (!user) {
      return false;
    }

    // Find the index of the order to delete
    const orderIndex = user.myOrders.findIndex(order => order._id.equals(orderId));

    if (orderIndex === -1) {
      return false;
    }

    // Calculate the subTotal of the order to be deleted
    const subTotal = user.myOrders[orderIndex].subTotal;

    // Update the totalAmount and delete the order
    user.totalAmount -= subTotal;
    user.myOrders.splice(orderIndex, 1);

    // Save the updated user data
    await user.save();
    return true;
  } catch (error) {
    console.error(error);
    return false;
  }
};