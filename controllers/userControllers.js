const Users = require("../models/users.js");
const Products = require("../models/products.js");
const { addOrder } = require('../controllers/orderControllers.js');
const Orders = require("../models/orders.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const { update } = require("../models/users.js");
const { response } = require("express");

// Registration
module.exports.registerUser = (req, res) => {

    let newUser = new Users ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo,
    })

    return newUser.save()
	.then(user => {
		console.log(user);
		// send "true" if the user is created
		res.send(user);
	})
	.catch(error =>{
		console.log(error);
		// send "false" if any error is encountered.
		res.send(false);
	})
}

// Login
module.exports.loginUser = (req, res) => {
    return Users.findOne({email: req.body.email})
    .then(result => {
		// User does not exists
		if(result == null){
			return res.send(false);
		}
		// User exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(result)});
			}
			else{
				return res.send(false); 
			}
		}
	})
}

// Getting Profile in Database
module.exports.getProfile = (usersId, gettingDataByAdmin) => {
    if(gettingDataByAdmin.isAdmin == true){
	return Users.findById(usersId)
    .then((result, err) => {
		if(err){
			    return false;
		    }
            else{
                result.password = '*****';
                return result;
            }
        })
    }
    else{
		return message.then((value) => {return value});
	}
}

// getting All Users
module.exports.getAllUsers = (req, res) =>{
	return Users.find({}).then(result => res.send(result));
}

// getting Profile without ID
module.exports.getManualProfile = (req, res) => {
		
    const userData = auth.decode(req.headers.authorization);

    console.log(userData);

    return Users.findById(userData.id).then(result =>{
        result.password = "***";
        res.send(result);
    })
}

// Checking Email
module.exports.checkEmailExists = (req, res) =>{
    return Users.find({email: req.body.email}).then(result =>{
	    console.log(result);
	        if(result.length > 0){
		        return res.send(true);
	        }
	        else{
		        return	res.send(false);
	        }
        })
    .catch(error => res.send(error));
}

// getting specific order of specific user
module.exports.getUserOrders = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  const user = await Users.findById(userData.id).populate("myOrders.products");

  const orders = user.myOrders.map((order) => {

    return {
      purchasedOn: order.purchasedOn,
      totalAmount: order.totalAmount,
      subTotal: order.subTotal,
      products: order.products.map((product) => {
        return {
          name: product.productName,
          price: product.price,
          quantity: product.quantity,
          subTotal: product.subTotal,
        };
      }),
      subTotal: order.subTotal
    };
  });

  const totalAmount = user.totalAmount;

  const responseData = {
    name: `${user.firstName} ${user.lastName}`,
    totalAmount: user.totalAmount,
    orders: orders,
  };

  console.log(responseData);
  res.send(responseData);
};





// getting all orders of all users:
module.exports.getAllOrders = async (req, res) => {
    const users = await Users.find({});
    const ordersByUser = {};

    for (const user of users) {
      ordersByUser[user._id] = {
        name: `${user.firstName} ${user.lastName}`,
        totalAmount: user.totalAmount,
        orders: []
      };

      for (const order of user.myOrders) {
        const orderData = {
          purchasedOn: order.purchasedOn,
          subTotal: order.subTotal,
          products: order.products
        };
        
        ordersByUser[user._id].orders.push(orderData);

        ordersByUser[user._id].totalAmount = user.totalAmount;
      }
    }

    res.send(ordersByUser);
};

// // Change to Admin Status
// module.exports.setStatus = async (req, res) => {
//   const userData = auth.decode(req.headers.authorization);
//   let newAdmin = {
//     usersId: req.body.usersId
//   }
//   if (userData.isAdmin == true) {
//     try {
//       const result = await Users.findByIdAndUpdate(newAdmin.usersId);
//       result.isAdmin == true;
//       await result.save();
//       res.send(true);
//     } catch (err) {
//       console.log(err);
//     }
//   }
// }
