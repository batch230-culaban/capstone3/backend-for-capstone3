const mongoose = require("mongoose");
const Products = require("../models/products");
const Order = require("../models/orders")
const auth = require("../auth.js");


//Creating a Product
module.exports.createProduct = (requestBody, createProductByAdmin) => {
    if(createProductByAdmin.isAdmin == true){

        let newProduct = new Products ({
            name: requestBody.name,
            description: requestBody.description,
            price: requestBody.price,
            stocks: requestBody.stocks,
            isActive: requestBody.isActive,
            productImage: requestBody.productImage
        })

        return newProduct.save()
        .then((newProduct, error) =>
        {
            if(error){
                return error;
            }
            else{
                return newProduct;
            }
        })
    }
    else{
        return message.then((value) => {return value});
    }
}

// Getting All Products
module.exports.getAllActiveProducts = () => {
    return Products.find({isActive: true}) // Only Active products
    .then (result => {
        return result;
    })
}

// Getting All Products
module.exports.getAllProducts = () => {
    return Products.find({}) // Only Active products
    .then (result => {
        return result;
    })
}



// Retrieving Single Product
module.exports.getProduct = (productId) => {
    return Products.findById(productId)
    .then((result, error) => {
        if(error){
            return  false;
        }
        else{
            return result;
        }
    })
}

// Update Products via Admin User
module.exports.updateProducts = (productId, updateProductData) => {
    if(updateProductData.isAdmin == true){
        return Products.findByIdAndUpdate(productId,
            {
                name: updateProductData.Products.name,
                description: updateProductData.Products.description,
                price: updateProductData.Products.price,
                stocks: updateProductData.Products.stocks,
                isActive: updateProductData.Products.isActive
            })
            .then((result, error) => {
                if(error){
                    return false;
                }
                return result;
            })
    }
    else{
		return message.then((value) => {return value});
	} 
}


// Product Archive by Admin User
module.exports.archiveProduct = (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	let updateIsActiveField = {
		isActive: req.body.isActive
	}

	if(userData.isAdmin){
		return Products.findByIdAndUpdate(req.params.productId, updateIsActiveField)
		.then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		})
	}
	else{
		return res.send(false);
	}
}

// Delete a product
exports.deleteProduct = async (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    if(userData.isAdmin){
      return Products.findByIdAndDelete(req.params.productId)
      .then(result => {
        console.log(result);
        res.send(true);
      })
      .catch(error =>{
        console.log(error);
        res.send(false);
      })
    }
    else{
        return res.send(false);
    }
  };