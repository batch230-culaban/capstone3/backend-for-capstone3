const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");

// add to Cart
router.post("/add", orderController.addOrderAndUpdateProduct);

// deleting order
router.delete("/orders/:userId/:orderId", orderController.deleteOrder);

module.exports = router;