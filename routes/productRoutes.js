const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth.js");
const { response } = require("express");
const Products = require("../models/products");


// Creating a Product but need Admin Authorization
router.post("/create", auth.verify, (request, response) =>
{
    const createProductByAdmin = {
        isAdmin : auth.decode(request.headers.authorization).isAdmin
    }
    productControllers.createProduct(request.body, createProductByAdmin)
    .then(resultFromController => response.send(resultFromController))
})

// Getting All Products that are Active
router.get("/active", (request, response) =>
    productControllers.getAllActiveProducts()
    .then(resultFromController => response.send(resultFromController))
)

// Getting All Products
router.get("/all", (request, response) =>
    productControllers.getAllProducts()
    .then(resultFromController => response.send(resultFromController))
)

// Retrieving Specific Product
router.get("/:productId", (request, response) => {
    productControllers.getProduct(request.params.productId)
    .then(resultFromController => response.send(resultFromController))
})

// Update Product Routes
router.put("/:productId/Update", auth.verify, (request, response) =>
{
    const updateProductData = {
        Products: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    productControllers.updateProducts(request.params.productId, updateProductData)
    .then(resultFromController => {response.send(resultFromController)
    })
})

// Archive Specific Product via Admin User
router.patch("/Archive/:productId", auth.verify, productControllers.archiveProduct);

// DELETE /products/:id
router.delete('/delete/:productId', auth.verify, productControllers.deleteProduct);

module.exports = router;