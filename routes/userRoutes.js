const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const userControllers = require("../controllers/userControllers.js");


// Register Routes
router.post("/register", userControllers.registerUser);

// Getting Profile Routes
router.get("/:usersId/userDetails", auth.verify, (request, response) => {
    
    const gettingDataByAdmin = {
        isAdmin : auth.decode(request.headers.authorization).isAdmin
    }
    
    userControllers.getProfile(request.params.usersId, gettingDataByAdmin)
    .then(resultFromController => 
        response.send(resultFromController))
})

// Login Routes
router.post("/login", userControllers.loginUser);

// set Status
// router.patch("/Promotion", auth.verify, userControllers.setStatus);

// Getting all Users
router.get("/allUsers", userControllers.getAllUsers);

// Checking Existing Email Manually
router.post("/checkEmail", userControllers.checkEmailExists);

// Getting Specific User Manually
router.get("/details", auth.verify, userControllers.getManualProfile);

// Get all orders for all users
router.get("/orders", userControllers.getAllOrders);

// Get orders for a specific user
router.get("/myOrders", auth.verify, userControllers.getUserOrders);

module.exports = router;