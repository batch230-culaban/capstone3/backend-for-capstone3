const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes.js");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/Users", userRoutes);
app.use("/Products", productRoutes);
app.use("/orders", orderRoutes);

// Connect to my MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@batch230.ofbpx0r.mongodb.net/CulabanECommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Culaban-Mongo DB Atlas"));

app.listen(process.env.PORT || 4000, () => 
    { console.log(`API is now online on port ${process.env.PORT || 4000} `)
});
