const jwt = require("jsonwebtoken");
const secret = "Capstone2ProductBookingAPI";

module.exports.createAccessToken = (users) => {
    const data = {
        id: users._id,
        email: users.email,
        isAdmin: users.isAdmin
    }
    return jwt.sign(data, secret, {});
}

module.exports.verify = (request, response, next) => {
    let token = request.headers.authorization
    if( typeof token !== "undefined"){
        console.log(token);

        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (error, data) => {
            if (error) {
                return response.send({
                    auth: "Failed"
                });
            }
            else{
                next();
            }
        })
    }
}

module.exports.decode = (token) => {
    if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
	}
	return jwt.verify(token, secret, (error, data) => {
		if(error){
			return null
		}
		else{
			return jwt.decode(token, {complete:true}).payload
		}
	})
}