const mongoose = require("mongoose");
const orderSchema = require("./orders");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First Name is Required"]
  },
  lastName: {
    type: String,
    required: [true, "Last Name is Required"]
  },
  email: {
    type: String,
    required: [true, "Email is Required"]
  },
  password: {
    type: String,
    required: [true, "Password is Required"]
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile Number is Required"]
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  myOrders: [
    {
      products: [orderSchema],
      purchasedOn: {
        type: Date,
        default: new Date()
      },
      subTotal: {
        type: Number
      }
    }
  ],
  totalAmount: {
    type: Number,
    default: 0
  }
});

module.exports = mongoose.model("User", userSchema);