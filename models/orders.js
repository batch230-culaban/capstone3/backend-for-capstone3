const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  productId: {
    type: String
  },
  productName: {
    type: String
  },
  quantity: {
    type: Number,
    required: [true, "Quantity is Required"]
  }
});

module.exports = orderSchema;